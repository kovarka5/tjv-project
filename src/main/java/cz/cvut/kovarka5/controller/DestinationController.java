package cz.cvut.kovarka5.controller;

import java.util.List;

import cz.cvut.kovarka5.entity.Destination;
import cz.cvut.kovarka5.repository.DestinationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
@RestController
@RequestMapping("/destinations")
public class DestinationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DestinationController.class);

    @Autowired
    private DestinationRepository destinationRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    List<Destination> list() {
        return destinationRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    Destination detail(@PathVariable Long id) {
        return destinationRepository.findOne(id);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable Long id) {
        LOGGER.info("Delete destination with id {}", id);
        destinationRepository.delete(id);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    void update(@PathVariable Long id,
            @RequestBody Destination destination) {
        LOGGER.info("Update destination with id {}", id);
        destinationRepository.update(id, destination.getName(), destination.getCoords());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Destination create(@RequestBody Destination destination) {
        LOGGER.info("Create destination {}", destination);
        return destinationRepository.save(destination);
    }
}
