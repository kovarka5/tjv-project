package cz.cvut.kovarka5.controller;

import java.util.List;

import cz.cvut.kovarka5.dto.FlightDto;
import cz.cvut.kovarka5.service.FlightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
@RestController
@RequestMapping("/flights")
public class FlightController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlightController.class);

    @Autowired
    private FlightService flightService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    List<FlightDto> list() {
        return flightService.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    FlightDto detail(@PathVariable Long id) {
        return flightService.findOne(id);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable Long id) {
        LOGGER.info("Delete flight with id {}", id);
        flightService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    FlightDto create(@RequestBody FlightDto flight) {
        LOGGER.info("Create flight {}", flight);
        return flightService.save(flight);
    }
}
