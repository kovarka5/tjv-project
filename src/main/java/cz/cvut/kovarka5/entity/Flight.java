package cz.cvut.kovarka5.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
@Entity(name = "FLIGHT")
public class Flight {

    @Id
    @GeneratedValue
    private Long id;
    //TODO (kkovarik, 15.11.2016, TASK) add serializer for Jackson
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "from_destination")
    private Destination fromDestination;

    @ManyToOne
    @JoinColumn(name = "to_destination")
    private Destination toDestination;
    private String company;
    private String flightNumber;
    private String additionalDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Destination getFromDestination() {
        return fromDestination;
    }

    public void setFromDestination(Destination fromDestination) {
        this.fromDestination = fromDestination;
    }

    public Destination getToDestination() {
        return toDestination;
    }

    public void setToDestination(Destination toDestination) {
        this.toDestination = toDestination;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAdditionalDescription() {
        return additionalDescription;
    }

    public void setAdditionalDescription(String additionalDescription) {
        this.additionalDescription = additionalDescription;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("date", date)
                .append("fromDestination", fromDestination)
                .append("toDestination", toDestination)
                .append("company", company)
                .append("flightNumber", flightNumber)
                .append("additionalDescription", additionalDescription)
                .toString();
    }
}
