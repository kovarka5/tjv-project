package cz.cvut.kovarka5.repository;

import cz.cvut.kovarka5.entity.Destination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
public interface DestinationRepository extends JpaRepository<Destination, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE DESTINATION d SET d.name = :name, d.coords = :coords WHERE d.id = :id")
    int update(@Param("id") Long id, @Param("name") String name, @Param("coords") String coords);
}
