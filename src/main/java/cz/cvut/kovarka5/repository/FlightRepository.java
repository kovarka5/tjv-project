package cz.cvut.kovarka5.repository;

import cz.cvut.kovarka5.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
public interface FlightRepository extends JpaRepository<Flight, Long> {

}
