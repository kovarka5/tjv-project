package cz.cvut.kovarka5.dto;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
public class FlightDto {

    private Long id;
    private Long fromDestination;
    private String fromDestinationName;
    private Long toDestination;
    private String toDestinationName;
    private String company;
    private String flightNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromDestination() {
        return fromDestination;
    }

    public void setFromDestination(Long fromDestination) {
        this.fromDestination = fromDestination;
    }

    public Long getToDestination() {
        return toDestination;
    }

    public void setToDestination(Long toDestination) {
        this.toDestination = toDestination;
    }

    public String getFromDestinationName() {
        return fromDestinationName;
    }

    public void setFromDestinationName(String fromDestinationName) {
        this.fromDestinationName = fromDestinationName;
    }

    public String getToDestinationName() {
        return toDestinationName;
    }

    public void setToDestinationName(String toDestinationName) {
        this.toDestinationName = toDestinationName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("fromDestination", fromDestination)
                .append("fromDestinationName", fromDestinationName)
                .append("toDestination", toDestination)
                .append("toDestinationName", toDestinationName)
                .append("company", company)
                .append("flightNumber", flightNumber)
                .toString();
    }

    public static final class Builder {

        private Long id;
        private Long fromDestination;
        private String fromDestinationName;
        private Long toDestination;
        private String toDestinationName;
        private String company;
        private String flightNumber;

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withFromDestination(Long fromDestination) {
            this.fromDestination = fromDestination;
            return this;
        }

        public Builder withFromDestinationName(String fromDestinationName) {
            this.fromDestinationName = fromDestinationName;
            return this;
        }

        public Builder withToDestination(Long toDestination) {
            this.toDestination = toDestination;
            return this;
        }

        public Builder withToDestinationName(String toDestinationName) {
            this.toDestinationName = toDestinationName;
            return this;
        }

        public Builder withCompany(String company) {
            this.company = company;
            return this;
        }

        public Builder withFlightNumber(String flightNumber) {
            this.flightNumber = flightNumber;
            return this;
        }

        public FlightDto build() {
            FlightDto flightDto = new FlightDto();
            flightDto.setId(id);
            flightDto.setFromDestination(fromDestination);
            flightDto.setFromDestinationName(fromDestinationName);
            flightDto.setToDestination(toDestination);
            flightDto.setToDestinationName(toDestinationName);
            flightDto.setCompany(company);
            flightDto.setFlightNumber(flightNumber);
            return flightDto;
        }
    }
}
