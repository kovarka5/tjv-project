package cz.cvut.kovarka5.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import cz.cvut.kovarka5.dto.FlightDto;
import cz.cvut.kovarka5.entity.Destination;
import cz.cvut.kovarka5.entity.Flight;
import cz.cvut.kovarka5.repository.DestinationRepository;
import cz.cvut.kovarka5.repository.FlightRepository;
import cz.cvut.kovarka5.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private DestinationRepository destinationRepository;

    @Override
    public List<FlightDto> findAll() {
        return flightRepository.findAll().stream()
                .map(FlightServiceImpl::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public FlightDto findOne(Long id) {
        return fromEntity(flightRepository.findOne(id));
    }

    @Override
    public FlightDto save(FlightDto dto) {
        return fromEntity(flightRepository.save(
                toEntity(
                        dto,
                        destinationRepository.findOne(dto.getFromDestination()),
                        destinationRepository.findOne(dto.getToDestination()))
                )
        );
    }

    @Override
    public void delete(Long id) {
        flightRepository.delete(id);
    }

    private static FlightDto fromEntity(Flight entity) {
        return new FlightDto.Builder()
                .withId(entity.getId())
                .withCompany(entity.getCompany())
                .withFlightNumber(entity.getFlightNumber())
                .withFromDestination(entity.getFromDestination() != null ? entity.getFromDestination().getId() : null)
                .withFromDestinationName(entity.getFromDestination() != null ? entity.getFromDestination().getName() : null)
                .withToDestination(entity.getToDestination() != null ? entity.getToDestination().getId() : null)
                .withToDestinationName(entity.getToDestination() != null ? entity.getToDestination().getName() : null)
                .build();
    }

    private static Flight toEntity(FlightDto dto, Destination from, Destination to) {
        Flight ret = new Flight();
        ret.setFromDestination(from);
        ret.setToDestination(to);
        ret.setCompany(dto.getCompany());
        ret.setFlightNumber(dto.getFlightNumber());
        return ret;
    }
}
