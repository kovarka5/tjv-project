package cz.cvut.kovarka5.service;

import java.util.List;

import cz.cvut.kovarka5.dto.FlightDto;
import cz.cvut.kovarka5.entity.Flight;


/**
 * @author <a href="mailto:karel.kovarik@bsc-ideas.com">Karel Kovarik</a>
 */
public interface FlightService {

    List<FlightDto> findAll();

    FlightDto findOne(Long id);

    FlightDto save(FlightDto dto);

    void delete(Long id);
}
