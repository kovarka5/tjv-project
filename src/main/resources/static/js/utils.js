function toast(text) {
    console.log(text);
}

function toastError(text) {
    console.log(text);
}

var toDestination = function(_name, _coords, _country) {
    return {
        name: _name,
        countryCode: _country,
        coords : _coords
    }
};

var toFlight = function(_from, _to, _company, _number) {
    return {
        fromDestination: _from,
        toDestination: _to,
        flightNumber: _number,
        company: _company
    }
};