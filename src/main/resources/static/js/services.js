var baseUrl = ".";

/*********************************************************/
/* DESTINATION RELATED ENDPOINTS */
/*********************************************************/
var destinationListService = function(successCallback) {

    $.ajax({
        url: baseUrl + '/destinations',
        type: 'GET',
        dataType: "json",
        success: function(result) {
            successCallback(result);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chyba pri nacitani seznamu destinaci :/ " + errorThrown);
            toast("Nelze načíst destinace");
        }
    });
};

var destinationDetailService = function(destinationId, successCallback) {

    $.ajax({
        url: baseUrl + '/destinations/' + destinationId,
        type: 'GET',
        dataType: "json",
        success: function(result) {
            successCallback(result);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chyba pri nacitani detailu destinace :/ " + errorThrown);
            toast("Nelze načíst destinace");
        }
    });
};

var destinationCreateService = function(destinationObj, successCallback) {
    console.log("Create new destination");

    $.ajax({
        url: baseUrl + '/destinations',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(destinationObj),
        success: function() {
            successCallback();
            toast("Uloženo");
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chybka " + errorThrown + " status " + textStatus);
            toastError("Chyba při ukládání")
        }
    });
};

var destinationUpdateService = function(destinationId, destinationObj, successCallback) {
    console.log("Update destination withId " + destinationId);

    $.ajax({
        url: baseUrl + '/destinations/' + destinationId,
        type: 'PUT',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(destinationObj),
        success: function() {
            successCallback();
            toast("Uloženo");
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chybka " + errorThrown);
            toastError("Chyba při ukládání")
        }
    });
};

var destinationDeleteService = function(destinationId, successCallback, failCallback) {
    console.log("Delete destination withId " + destinationId);

    $.ajax({
        url: baseUrl + '/destinations/' + destinationId,
        type: 'DELETE',
        contentType: "application/json; charset=utf-8",
        success: function() {
            successCallback();
            toast("Smazáno");
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chybka " + errorThrown);
            failCallback(errorThrown);
            toastError("Chyba při mazání")
        }
    });
};

/*********************************************************/
/* FLIGHT RELATED ENDPOINTS */
/*********************************************************/

var flightListService = function(successCallback) {

    $.ajax({
        url: baseUrl + '/flights',
        type: 'GET',
        dataType: "json",
        success: function(result) {
            successCallback(result);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chyba pri nacitani seznamu destinaci :/ " + errorThrown);
        }
    });
};


var flightCreateService = function(flightObj, successCallback) {
    console.log("Create new flight");

    $.ajax({
        url: baseUrl + '/flights',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(flightObj),
        success: function() {
            successCallback();
            toast("Uloženo");
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chybka " + errorThrown + " status " + textStatus);
            toastError("Chyba při ukládání")
        }
    });
};

var flightUpdateService = function(flightId, flightObj, successCallback) {
    console.log("Update flight withId " + flightId);

    $.ajax({
        url: baseUrl + '/flights/' + flightId,
        type: 'PUT',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(flightObj),
        success: function() {
            successCallback();
            toast("Uloženo");
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("chybka " + errorThrown);
            toastError("Chyba při ukládání")
        }
    });
};

var flightDeleteService = function(flightId, successCallback, failCallback) {
    console.log("Delete flight withId " + flightId);

    $.ajax({
        url: baseUrl + '/flights/' + flightId,
        type: 'DELETE',
        contentType: "application/json; charset=utf-8",
        success: function() {
            successCallback();
            toast("Smazáno");
        },
        error: function(xhr, textStatus, errorThrown) {
            failCallback(errorThrown);
            toastError("Chyba při mazání")
        }
    });
};